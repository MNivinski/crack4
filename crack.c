#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"
#include "entry.h"

const int PASS_LEN=20;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

void findPass(char* hash, FILE* rb)
{
    //   Convert hex string to digest (use hex2digest. See md5.h)
    unsigned char* digest = hex2digest(hash);
    
    //   Extract the first three bytes
    //     Use those three bytes as a 24-bit number
    int idx = (digest[0]*65536) + (digest[1]*256) + digest[2];
    
    //   Seek to that location in the rainbow file
    fseek(rb, idx * sizeof(struct entry), SEEK_SET);
    
    //   Read the entry found there
    struct entry tempE;
    fread(&tempE, sizeof(struct entry), 1, rb);
    
    //   Check to see if it's the one you want
    if(strcmp(digest2hex(tempE.hash), hash) == 0)
    {
        printf("%s %s\n", hash, tempE.pass);
        return;
    }
    
    //     If not, read the next one
    //     Repeat until you find the one you want.
    //   Display the hash and the plaintext
    while(1)
    {
        fread(&tempE, sizeof(struct entry), 1, rb);
        if(strcmp(digest2hex(tempE.hash), hash) == 0)
        {
            printf("%s %s\n", hash, tempE.pass);
            return;
        }
    }
}

int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file rainbow_file\n", argv[0]);
        exit(1);
    }

    // TODO: Open the text hashes file for reading
    FILE *hf = fopen(argv[1], "r");
    // TODO: Check to make sure file was successfully opened
    if(!hf)
    {
        printf("Can't open that file\n");
        exit(1);
    }
    // TODO: Open the binary rainbow file for reading
    FILE *rb = fopen(argv[2], "r");
    // TODO: Check to make sure if was successfully opened
    if(!rb)
    {
        printf("Can't open that file\n");
        exit(1);
    }
    // TODO: This is a long list of things to do, so
    //       break it up into functions!
    
    // For each hash (read one line at a time out of the file):
    char str[HASH_LEN];
    while(fscanf(hf, "%s", str) != EOF)
    {
        findPass(str, rb);
    }
}
